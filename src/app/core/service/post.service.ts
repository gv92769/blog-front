import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../model/post';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/post';
  }

  listAll(): Observable<Post[]> {
    return this.http.get<Post[]>(this.url);
  }

  salvar(post: Post): Observable<Post> {
    return this.http.post<Post>(this.url, post);
  }

  delete(id: number) {
    return this.http.delete(this.url+'/'+id);
  }

}
