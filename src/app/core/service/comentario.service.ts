import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comentario } from '../model/comentario';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ComentarioService {

  private readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/comentario';
  }

  salvar(post: Comentario): Observable<Comentario> {
    return this.http.post<Comentario>(this.url, post);
  }

  delete(id: number) {
    return this.http.delete(this.url+'/'+id);
  }

}
