import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Album } from '../model/album';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  private readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.apiBaseUrl + '/album';
  }

  listAll(): Observable<Album[]> {
    return this.http.get<Album[]>(this.url);
  }

  salvar(album: Album): Observable<Album> {
    return this.http.post<Album>(this.url, album);
  }

  delete(id: number) {
    this.http.delete(this.url+'/'+id);
  }

}
