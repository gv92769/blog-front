import { Post } from "./post";
import { Usuario } from "./usuario";

export class Comentario {
    id: number;
    usuario: Usuario = new Usuario();
    post: Post = new Post();
    descricao: string;
}
