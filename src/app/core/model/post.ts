import { Usuario } from '../model/usuario';
import { Comentario } from './comentario';

export class Post {
  id: number;
  usuario: Usuario = new Usuario();
  descricao: string;
  comentarios: Comentario[];
}
