export class Usuario {
  id: number;
	nome: string;
	email: string;
	login: string;
	senha: string;
	ativo: boolean = true;
	perfis: string[];
}
