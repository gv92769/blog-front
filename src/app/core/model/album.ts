import { Usuario } from '../model/usuario';
import { Foto } from '../model/foto';

export class Album {
    id: number;
    usuario: Usuario = new Usuario();
    fotos: Foto[];
    imagens: any;
    descricao: string;
}
