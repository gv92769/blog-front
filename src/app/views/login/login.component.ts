import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { Usuario } from 'src/app/core/model/usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  usuario: Usuario = new Usuario();

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.deleteTokens();
  }

  onSubmit(){
    if (this.form.valid) {
      this.authService.login(this.usuario).subscribe(
        data =>{
          this.router.navigate(['/']);
        },
        error => {
            console.log(error);
        }
      );
    }
  }

}
