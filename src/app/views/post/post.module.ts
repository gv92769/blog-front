import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PostRoutingModule } from './post-routing.module';

import { AdicionarComponent } from './adicionar/adicionar.component';
import { ListaComponent } from './lista/lista.component';

@NgModule({
  declarations: [
    AdicionarComponent,
    ListaComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PostRoutingModule
  ]
})
export class PostModule { }
