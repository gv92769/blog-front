import { NgModule } from '@angular/core';
import { AdicionarComponent } from './adicionar/adicionar.component';
import { ListaComponent } from './lista/lista.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ListaComponent,
    data: {
      title: 'post'
    },
    children: [
      {
        path: 'adicionar',
        component: AdicionarComponent,
        data: {
          title: 'Adicionar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
