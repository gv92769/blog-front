import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as jwt_decode from 'jwt-decode';
import { Usuario } from 'src/app/core/model/usuario';
import { Post } from '../../../core/model/post';
import { PostService } from '../../../core/service/post.service';

@Component({
  selector: 'app-adicionar',
  templateUrl: './adicionar.component.html',
  styleUrls: ['./adicionar.component.css']
})
export class AdicionarComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  post: Post = new Post();

  constructor(private postService: PostService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.form.valid) {
      let usuario: Usuario = new Usuario();
      usuario.id = jwt_decode( sessionStorage.getItem('access_token')).id;
      this.post.usuario = usuario;
      this.postService.salvar(this.post)
        .subscribe(
          data => {
            this.post = data;
          },
          error => {
            console.log(error);
          }
        );
    }
  }

}
