import { Component, OnInit, ViewChild } from '@angular/core';
import { Post } from 'src/app/core/model/post';
import { PostService } from 'src/app/core/service/post.service';
import * as jwt_decode from 'jwt-decode';
import { ComentarioService } from 'src/app/core/service/comentario.service';
import { Comentario } from 'src/app/core/model/comentario';
import { Usuario } from 'src/app/core/model/usuario';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  posts: Post[] = new Array;

  idUsuario: number;

  comentario: Comentario = new Comentario();

  @ViewChild('form', { static: false }) form: FormGroup;

  constructor(private postService: PostService, private comentarioService: ComentarioService) { }

  ngOnInit(): void {
    this.idUsuario = jwt_decode( sessionStorage.getItem('access_token')).id;

    this.postService.listAll()
      .subscribe(
        data => { 
          this.posts = data;
        },
        error => {
          console.log(error);
        }
      );
  }

  delete(id: number) {
    this.postService.delete(id)
      .subscribe(
        data => {
          let index = this.posts.findIndex(x => x.id == id);
          this.posts.splice(index, 1);
        },
        error => {
          console.log(error);
        }
      );
  }

  deleteComentario(comentario: number, post: number) {
    this.comentarioService.delete(comentario)
      .subscribe(
        data => {
          let index = this.posts.findIndex(x => x.id == post);
          this.posts[index].comentarios.splice(this.posts[index].comentarios.findIndex(x => x.id == comentario), 1);
        },
        error => {
          console.log(error);
        }
      );
  }

  salvar(post: Post) {
    if (this.form.valid) {
      let usuario: Usuario = new Usuario();
      usuario.id = this.idUsuario;
      this.comentario.usuario = usuario;
      this.comentario.post = post;
      this.comentarioService.salvar(this.comentario)
        .subscribe(
          data => {
            let index = this.posts.findIndex(x => x.id == post.id);
            if (this.posts[index].comentarios == null)
              this.posts[index].comentarios = new Array;
            this.posts[index].comentarios.push(data);
            this.comentario = new Comentario();
          },
          error => {
            console.log(error);
          }
        );
    }
  }

}
