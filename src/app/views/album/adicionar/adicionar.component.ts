import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Album } from 'src/app/core/model/album';
import * as jwt_decode from 'jwt-decode';
import { Usuario } from 'src/app/core/model/usuario';
import { AlbumService } from 'src/app/core/service/album.service';

@Component({
  selector: 'app-adicionar',
  templateUrl: './adicionar.component.html',
  styleUrls: ['./adicionar.component.css']
})
export class AdicionarComponent implements OnInit {

  @ViewChild('form', { static: false }) form: FormGroup;

  album: Album = new Album();

  constructor(private albumService: AlbumService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.form.valid) {
      let usuario: Usuario = new Usuario();
      usuario.id = jwt_decode( sessionStorage.getItem('access_token')).id;
      this.album.usuario = usuario;
      this.albumService.salvar(this.album)
        .subscribe(
          data => {
            this.album = data;
          },
          error => {
            console.log(error);
          }
        );
    }
  }

  handleFileInput(files: any) {
    this.album.imagens = files;
  }

}
