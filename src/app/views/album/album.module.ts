import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumRoutingModule } from './album-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AdicionarComponent } from './adicionar/adicionar.component';

@NgModule({
  declarations: [
    AdicionarComponent,
  ],
  imports: [
    CommonModule,
    AlbumRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AlbumModule { }
