import { NgModule } from '@angular/core';
import { AdicionarComponent } from './adicionar/adicionar.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Album'
    },
    children: [
      {
        path: 'adicionar',
        component: AdicionarComponent,
        data: {
          title: 'Adicionar'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule { }
