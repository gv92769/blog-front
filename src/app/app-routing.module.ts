import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: 'logout',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    data: {
      title: 'Início'
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'album',
        loadChildren: () => import('./views/album/album.module').then(m => m.AlbumModule)
      },
      {
        path: 'post',
        loadChildren: () => import('./views/post/post.module').then(m => m.PostModule)
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }