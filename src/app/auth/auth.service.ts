import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from '../core/model/usuario';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly ACCESS_TOKEN = "access_token";
  
  private readonly REFRESH_TOKEN = "refresh_token";
  
  private readonly url: string;

  constructor(private http: HttpClient, private router: Router) {
    this.url = environment.apiBaseUrl + '/autenticacao';
  }

  isLoggedIn() : boolean {
    if(this.getAccessToken() !== null && this.getRefreshToken() !== null)
      return true;
    else
      return false
  }

  login(usuario: Usuario): Observable<string> {
    return this.http.post<any>(this.url + '/login',usuario).pipe(
      map(data => {
            let accessToken = data.access_token;
            let refreshToken = data.refresh_token;
            this.setAccessToken(accessToken);
            this.setRefreshToken(refreshToken);
            return accessToken;
          }
      )
    );
  }

  logout(): Observable<any> {
    this.deleteTokens();
    this.router.navigate(['/login']);
    return Observable.throw("logout");
  }

  refreshToken(): Observable<string> {
    let refreshToken = this.getRefreshToken();
    return this.http.post<any>(this.url + '/refreshtoken',refreshToken).pipe(
      map(data => {
          let accessToken = data.access_token;
          this.setAccessToken(accessToken);
          return accessToken;
      })
    )
  }

  deleteTokens(){
      this.deleteAccessToken();
      this.deleteRefreshToken();
  }

  getAccessToken(){
    return sessionStorage.getItem(this.ACCESS_TOKEN);
  }

  private getRefreshToken(){
    return sessionStorage.getItem(this.REFRESH_TOKEN);
  }

  private deleteAccessToken(){
    sessionStorage.removeItem(this.ACCESS_TOKEN);
  }

  private deleteRefreshToken(){
    sessionStorage.removeItem(this.REFRESH_TOKEN);
  }

  private setAccessToken(token){
    sessionStorage.setItem(this.ACCESS_TOKEN,token);
  }

  private setRefreshToken(token){
    sessionStorage.setItem(this.REFRESH_TOKEN,token);
  }
  
}
