import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse } from "@angular/common/http";

import { AuthService } from '../auth/auth.service';

import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/internal/operators/filter';
import { switchMap, take } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

@Injectable()

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  isRefreshingToken: boolean = false;

  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private authService: AuthService) {}

  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({ setHeaders: { Authorization: 'Bearer ' + token }})
  }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    if(req.url.includes('/autenticacao/login') || req.url.includes('/autenticacao/refreshtoken')){
      return next.handle(req);
    }

    return next.handle(this.addToken(req, this.authService.getAccessToken())).pipe(
      catchError(error => {
          if (error instanceof HttpErrorResponse) {
            if(error.error.status === 401){
              if(error.error.message == "expired_token"){
                return this.handle401ExpiredTokenError(req,next);
              }
            }
            return Observable.throw(error);
          } else {
            return Observable.throw(error);
        }
      }));
  }

  handle401ExpiredTokenError(req: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;
      this.tokenSubject.next(null);
      
      return this.authService.refreshToken().pipe(
        switchMap((newToken: string) => {
            if (newToken) {
                this.tokenSubject.next(newToken);
                return next.handle(this.addToken(req, newToken));
            }
            this.isRefreshingToken = false;

            return this.authService.logout();
        }),
        catchError(error => {
          this.isRefreshingToken = false;
          return this.authService.logout();
        })
      );
    } else {

        return this.tokenSubject.pipe(
            filter(token => token != null),
            take(1),
            switchMap(token => {
                return next.handle(this.addToken(req, token));
            })
          );
    }
  }
}
